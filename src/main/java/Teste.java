
import java.io.File;
import java.util.Scanner;
import utfpr.dainf.ct.ed.exemplo.Arvore;
import utfpr.dainf.ct.ed.exemplo.Diretorio;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Exemplo de percurso de diretório.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Teste {
    static Diretorio diretorio;
    
    public static void main(String[] args) {
        String dir;
        Scanner s = new Scanner(System.in);
        System.out.print("Diretório que deseja percorrer: ");
        dir = s.next();
        diretorio = new Diretorio(new File(dir));
        carregaDiretorio(diretorio);
        diretorio.percorre();
    }
    
    private static void carregaDiretorio(Arvore<File> raiz) {
        File[] arqs = raiz.getValor().listFiles();
        for (File f: arqs) {
            Arvore<File> filho = new Arvore<>(f);
            if (f.isDirectory()) { // diretórios antes de arquivos
                diretorio.addFilho(raiz, filho, 0);
                carregaDiretorio(filho); 
            } else {
                diretorio.addFilho(raiz, filho);
            }
        }
    }
}
