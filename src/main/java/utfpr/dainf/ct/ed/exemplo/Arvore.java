package utfpr.dainf.ct.ed.exemplo;

import java.util.ArrayList;
import java.util.List;

/**
 * Exemplo de implementação de árvore genérica.
 * 
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <E> Tipdo do elemento armazenado
 */
public class Arvore<E> {
    private Arvore<E> pai;
    private List<Arvore<E>> filhos;
    private E valor;

    /**
     * Cria uma árvore vazia
     */
    public Arvore() {
    }

    /**
     * Cria uma árvore com a raiz contendo o valor especificado.
     * @param valor O valor armazenado na raiz.
     */
    public Arvore(E valor) {
        this.valor = valor;
    }

    /**
     * Retorna o valor armazenado neste nó.
     * @return O valor armazendo neste nó.
     */
    public E getValor() {
        return valor;
    }

    /**
     * Inicializa o valor armazenado neste nó.
     * 
     */
    public void setValor(E valor) {
        this.valor = valor;
    }
    
    /**
     * Adiciona um nó ao nó pai especificado na posição especificada.
     * @param pai O nó pai.
     * @param filho O nó filho a ser adicionado.
     * @param pos A posição para inserção do nó filho.
     */
    public void addFilho(Arvore<E> pai, Arvore<E> filho, int pos) {
        if (pai.filhos == null) {
            pai.filhos = new ArrayList<>();
        }
        filho.pai = pai;
        pai.filhos.add(pos, filho);
    }
    
    /**
     * Adiciona um nó ao nó pai especificado no final da lista de filhos.
     * @param pai O nó pai.
     * @param filho O nó filho a ser adicionado.
     */
    public void addFilho(Arvore<E> pai, Arvore<E> filho) {
        addFilho(pai, filho, pai.filhos == null ? 0 : pai.filhos.size());
    }
    
    /**
     * Adiciona uma lista de filhos ao nó pai especificado na posição especificada.
     * @param pai O nó pai.
     * @param filhos Os nós filhos a serem adicionados.
     * @param pos A posição para inserção do nó filho.
     */
    public void addFilhos(Arvore<E> pai, List<Arvore<E>> filhos, int pos) {
        for (Arvore<E> f: filhos) {
            addFilho(pai, f, pos);
        }
    }

    /**
     * Adiciona uma lista de filhos ao nó pai especificado ao final da lista de filhos.
     * @param pai O nó pai.
     * @param filhos Os nós filhos a serem adicionados.
     */
    public void addFilhos(Arvore<E> pai, List<Arvore<E>> filhos) {
        addFilhos(pai, this.filhos,  pai.filhos == null ? 0 : pai.filhos.size());
    }
    
    /**
     * Método a ser sobrecarregado.
     * Implementa a visita ao nó.
     * @param no O nó visitado 
     */
    public void visita(Arvore<E> no) {  
    }
    
    /**
     * Percorre a árvore em profundidade a partir do nó especificado.
     * @param no O nó inicial do percurso.
     */
    protected void percorre(Arvore<E> no) {
        if (no != null) {
            visita(no);
            if (no.filhos != null) {
                for (Arvore<E> filho: no.filhos) {
                    percorre(filho);
                }
            }
        }
    }
    
    /**
     * Percorre a árvore em profundidade a partir deste nó.
     */
    public void percorre() {
        percorre(this);
    }
}
