package utfpr.dainf.ct.ed.exemplo;

import java.io.File;

/**
 * Montagem de uma ãrvore a partir de uma estrutura de diretórios.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Diretorio extends Arvore<File> {

    public Diretorio(File raiz) {
        super(raiz);
    }

    @Override
    public void visita(Arvore<File> no) {
        if (no.getValor().isFile()) {
            System.out.print("\t");
        }
        System.out.println(no.getValor().getAbsolutePath());
    }
    
}
